import { Books } from '../models/Book';
import { Cd } from '../models/Cd';

export class MediaService {
   
    bookList: Books[] = [
        {
        name: 'Les fourmis',
        auteur: 'Bernard Werber',
        isLend: true
    },
        {
        name: 'Mucha',
        auteur: 'Daniel Kiecol',
        isLend: true
    },
        {
        name: 'Dali',
        auteur:'Dali',
        isLend: true
    },
        {
        name: 'Axolot',
        auteur: 'Patrick Baud',
        isLend: false
    }
    ] 

    cdList: Cd[] = [
        {
        artiste: 'Rolling Stones',
        style: 'Rock',
        isLend: false
    },
        {
        artiste: 'AlTarba',
        style: 'Electro/Rap',
        isLend: false
    },
        {
        artiste: 'Emir Kusturica',
        style:'Various',
        isLend: true
    },
        {
        artiste: 'Bawdy Festival',
        style: 'Clown core',
        isLend: false
    }
    ]

}