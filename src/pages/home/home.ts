import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BookListPage } from '../book/book';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {}

  onGoToBook(){
    this.navCtrl.push(BookListPage);
  }
}
