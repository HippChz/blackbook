import { Component } from '@angular/core';
import { ModalController, MenuController} from 'ionic-angular';
import { Books } from '../../models/Book';
import { MediaService } from '../../services/media.service';
import { LendBookPage } from '../book/lend-book/lend-book';



@Component ({
    selector: 'page-book',
    templateUrl: 'book.html'
})

export class BookListPage {

   bookList: Books[];

    constructor(private modalCtrl: ModalController,
                private mediaService: MediaService,
                private menuCtrl: MenuController ){}

    ionViewWillEnter(){
        this.bookList = this.mediaService.bookList.slice();
    }            

    onLoadBook(index: number){
        let modal = this.modalCtrl.create(LendBookPage, {index: index});
        modal.present();
    }

    onTogglemenu(){
        this.menuCtrl.open();
    }

}