import { Component, OnInit} from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Books } from '../../../models/Book';
import { MediaService} from '../../../services/media.service';


@Component ({
    selector: 'page-lend-book',
    templateUrl: 'lend-book.html'
})

export class LendBookPage implements OnInit {
    
    index: number;
    book: Books;

    constructor(public navParams: NavParams,
                public viewCtrl: ViewController,
                public mediaService: MediaService){}

    ngOnInit() {
        this.index = this.navParams.get('index');
        this.book = this.mediaService.bookList[this.index];
    }

    dismissModal(){
        this.viewCtrl.dismiss();
    }

    onLendBook(){
        this.book.isLend = !this.book.isLend;
    }
}