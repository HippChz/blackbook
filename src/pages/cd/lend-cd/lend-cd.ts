import { Component, OnInit } from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import { Cd } from '../../../models/Cd';
import { MediaService } from './../../../services/media.service';


@Component ({
    selector: 'page-lend-cd',
    templateUrl: 'lend-cd.html'
})

export class LendCdPage implements OnInit {

    index: number;
    cd: Cd;

    constructor(public navParams: NavParams,
                public viewCtrl: ViewController,
                public mediaService: MediaService){}

    ngOnInit() {
        this.index = this.navParams.get('index');
        this.cd = this.mediaService.cdList[this.index];
    }

    dismissModal(){
        this.viewCtrl.dismiss();
    }

    onLendCd(){
        this.cd.isLend = !this.cd.isLend;
    }
}