import { Component } from '@angular/core';
import { ModalController,MenuController} from 'ionic-angular';
import { Cd } from '../../models/Cd';
import { MediaService } from './../../services/media.service';
import { LendCdPage } from '../cd/lend-cd/lend-cd';


@Component ({
    selector: 'page-cd',
    templateUrl: 'cd.html'
})

export class CdListPage {

    cdList: Cd[];

    constructor(private modalCtrl : ModalController,
                private mediaService: MediaService,
                private menuCtrl: MenuController){}

    ionViewWillEnter(){
        this.cdList = this.mediaService.cdList.slice();
    }

    onLoadCd(index:number){
        let modal = this.modalCtrl.create(LendCdPage, {index : index});
        modal.present();
    }  

    onTogglemenu(){
        this.menuCtrl.open();
    }
}
