import { Component} from '@angular/core';
import { BookListPage } from '../book/book';
import { CdListPage } from '../cd/cd';

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
})

export class TabsPage {
    bookListPage = BookListPage;
    cdListPage = CdListPage;
}