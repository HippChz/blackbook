export class Cd {
    style: string;
    isLend: boolean;
  
    constructor(public artiste: string) {
      this.isLend = false;
    }
  }